import java.util.Random;

public class RouletteWheel {
    private int number = 0;
    private Random rand;

    public RouletteWheel() {
        rand = new Random();
    }

    public void spin() {
        this.number = rand.nextInt(37);
    }

    public int getValue() {
        return this.number;
    }

}